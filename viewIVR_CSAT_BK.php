<?php
 // FECHA Y HORA
 $dateStart = $_GET['dateStart'];
 $dateEnd = $_GET['dateEnd'];
 $timeStart = $_GET['timeStart'];
 $timeEnd = $_GET['timeEnd'];
    include_once('head.php');
?>
<br>
<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<style>
        .th1{
            background: #909497;
            color: #fdfefe;
            font-size: 15px;
        }
        .th2{
            background: #2471a3;
            color: #fdfefe;
            font-size: 15px;
        }
        .th3{
            background: #626567 ;
            color: #fdfefe;
            font-size: 13px;
        }
        .td1{
            font-size: 16px;
        }

</style>

<body>
        <div class="row">
        <div class="col-6 col-md-1"></div>
        <div class="col-6 col-md-10">

            <h3 class="text-center"> Report: IVR CSAT <strong> <?php echo $newDate?> </strong> </h3>   
            <br>
            <div class="text-center">
                <a href="index.php" class="btn btn-info" id="submit"> <i class=""></i> Back </a>
                <a href="#" class="btn btn-success" id="submitExport">
                    <i class="fa fa-download"></i> Export to Excel
                </a>
                </p>
                <form action="exportIVR_CSAT.php" method="post" target="_blank" id="formExport">
                    <input type="hidden" id="data_to_send" name="data_to_send" />
                </form>
            </div>

            <br><br>
            <table class="table table-hover table-bordered" border="1" bordercolor="#666666" id="export_to_excel" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <th colspan="7" class=" th3 text-center">IVR Questions</th>
                        <th class="th1 text-center">Knowledge</th>
                        <th class="th1 text-center">Experience with Staff</th>
                        <th class="th1 text-center">Courtesy</th>
                        <th class="th1 text-center">Waiting Time</th>
                    </tr>
                    <tr>
                        <th class="th2 text-center">Source Type</th>
                        <th class="th2 text-center">Response Date (dd-MM-YYY)<br/> </th>
                        <th class="th2 text-center">Region</th>
                        <th class="th2 text-center">Country</th>
                        <th class="th2 text-center">Location</th>
                        <th class="th2 text-center">Mission</th>
                        <th class="th2 text-center">Overall Rating</th>
                        <th class="th2 text-center">The contact centre officer was knowledgeable and well trained</th>
                        <th class="th2 text-center">The contact centre officer understood my issue</th>
                        <th class="th2 text-center">The contact centre officer was courteous and polite</th>
                        <th class="th2 text-center">Once connected to the contact centre officer, Your call was handled at an appropriate speed</th>
                    </tr>
                </thead>
                    <?php
                        include('conexion.php');
                            //SELECT PAISES
                            $query = "SELECT DISTINCT (omnifon.UniqueId),
                            DATE_FORMAT(SUBSTRING(cdr.calldate, 1, 10),'%d-%m-%Y') As Date,
                            SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',1),1,20) As Country,
                            GROUP_CONCAT(omnifon.Pregunta, omnifon.Respuesta ORDER By omnifon.Pregunta) As optionQuestion
                            FROM omnifon.Resultados As omnifon
                            INNER JOIN asteriskcdrdb.cdr As cdr ON(cdr.uniqueid = omnifon.UniqueId)
                            WHERE cdr.dcontext='Encuesta_Omnifon'
                            AND calldate >= '$dateStart $timeStart' AND calldate <= '$dateEnd $timeEnd'
                            GROUP BY UniqueId
                            ORDER BY cdr.calldate ASC;";
                            $resultadoPais =$mysqli -> query($query);
                            //echo "Query: ".$query;

                            while ($rowPais=mysqli_fetch_array($resultadoPais)){
                            $date= $rowPais["Date"];
                            $country = $rowPais['Country'];
                            $optionQuestion = $rowPais['optionQuestion'];

                            $Q1 = substr($optionQuestion, 0, 1);
                            $A1 = substr($optionQuestion, 1, 1);

                            $Q2 = substr($optionQuestion, 3, 1);
                            $A2 = substr($optionQuestion, 4, 1);

                            $Q3 = substr($optionQuestion, 6, 1);
                            $A3 = substr($optionQuestion, 7, 1);

                            $Q4 = substr($optionQuestion, 9, 1);
                            $A4 = substr($optionQuestion, 10, 1);

                            $Q5 = substr($optionQuestion, 12, 1);
                            $A5 = substr($optionQuestion, 13, 1);

                            if($A1 == ""){
                                $A1 = '0';
                            }
                            if($A2 == ""){
                                $A2 = '0';
                            }
                            if($A3 == ""){
                                $A3 = '0';
                            }
                            if($A4 == ""){
                                $A4 = '0';
                            }
                            if($A5 == ""){
                                $A5 = '0';
                            }

                    ?>
                <tbody>
                    <tr>
                        <td class="td1 text-center">IVR</td>
                        <td class="td1 text-center"><?php echo $date ?></td>
                        <td class="td1 text-center">China</td>
                        <td class="td1 text-center"><?php echo $country ?></td>
                        <td class="td1 text-center">Call Center (Omniphonia)</td>
                        <td class="td1 text-center">Canada</td>
                        <td class="td1 text-center"><?php echo $A1 ?></td>
                        <td class="td1 text-center"><?php echo $A2 ?></td>
                        <td class="td1 text-center"><?php echo $A3 ?></td>
                        <td class="td1 text-center"><?php echo $A4 ?></td>
                        <td class="td1 text-center"><?php echo $A5 ?></td>
                    </tr>
                </tbody>
                <?php
                        }
                ?>
            </table>
        </div>
  <div class="col-6 col-md-1">
  </div>
</div>

<div class="row">
        <div class="col-lg-12">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Bloque de anuncios adaptable -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-6676636635558550"
                 data-ad-slot="8523024962"
                 data-ad-format="auto"
                 data-full-width-responsive="true"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</div>

<script src="js/app.js"></script>
</body>
</html>


        
            
        

