<html>
       <?php
        include_once('head.php'); 

        $date = date("Y-m-d");
        //Si selecciono el boton View
        if(isset($_GET['view'])){
            //echo "Click en boton VIEW";
            $dateStart = $_GET['dateStart'];
            $dateEnd = $_GET['dateEnd'];
            $timeStart = $_GET['timeStart'];
            $timeEnd = $_GET['timeEnd'];
            $reporte = $_GET['reporte'];

            if(($reporte == 1 ) && $dateStart > $dateEnd){
                echo  '<div class="container">
                            <div class="alert alert-danger">
                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                            <strong>Invalid date range, Date start cannot be greater than Date end</strong>
                            </div>
                        </div>';
            }
            elseif(($reporte == 1 ) && $dateStart > $date){
                echo '<div class="container">
                        <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Invalid date range, Date start cannot be greater than current date</strong>
                        </div>
                    </div>';
            }

            elseif(($reporte == 1 ) && $dateEnd > $date){
                echo '<div class="container">
                        <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Invalid date range, Date end cannot be greater than current date</strong>
                        </div>
                    </div>';
            }
                elseif($reporte == 1 ){
                    header("Location: viewIVR_CSAT.php?dateStart=$dateStart&dateEnd=$dateEnd&timeStart=$timeStart&timeEnd=$timeEnd&reporte=$reporte");
                }
            }
    ?>

    <body>
       
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="get"> 
            <div class="container">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Welcome!</strong>
                </div>  
                <br>
                <h6>Select a date range to get your report</h6>
                <p></p>
                <div class="row">
                    <div class="col-sm-3">
                       <h6>Date start: </h6>
                        <input name="dateStart" type="date" class="form-control" required>
                        <br>
                        <h6>Date end: </h6>
                        <input name="dateEnd" type="date" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <h6>Time start:</h6>
                        <input name="timeStart" class="form-control" type="time">
                        <br>
                        <h6>Time end:</h6>
                        <input name="timeEnd" class="form-control" type="time">
                    </div>
                    <div class="col-sm-3">
                        <h6>Select a report</h6>
                        <select name="reporte" id="reporte" class="form-control">
                            <option name="1" value="1">IVR_CSAT_Report</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <h6>Select a option</h6>
                        <!-- <button type="submit" name="download" class="btn btn-success">Download</button> -->
                        <button type="submit" name="view" class="btn btn-success">View</button>
                    </div>
                </div>
            </div>
        </form>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>