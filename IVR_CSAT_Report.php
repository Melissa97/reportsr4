<?php

 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
     
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'IVR Questions')
                ->setCellValue('H1', 'Knowledge')
                ->setCellValue('I1', 'Experience with Staff')
                ->setCellValue('J1', 'Courtesy')
                ->setCellValue('K1', 'Waiting Time')
                ->setCellValue('A2', 'Source Type')
                ->setCellValue('B2', 'Date')
                ->setCellValue('C2', 'Region')
                ->setCellValue('D2', 'Country')
                ->setCellValue('E2', 'Location')
                ->setCellValue('F2', 'Mission')
                ->setCellValue('G2', "Overall \rRaiting")
                ->setCellValue('H2', "The contact centre officer \rwas knowledgeable and well trained")
                ->setCellValue('I2', "The contact centre officer\runderstood my issue")
                ->setCellValue('J2', "The contact centre officer\r was courteous and polite")
                ->setCellValue('K2', "Once connected to the contact centre officer,\r Your call was handled at an appropriate speed");
                

    $boldArray = array('font' => array('bold' => true, 'color' => array('rgb' => 'FFFFFF')),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1); $objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->setWrapText(true);  
    $objPHPExcel->getActiveSheet()->getStyle('A1:K2')->applyFromArray($boldArray);

    //Ancho de las columnas

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(19);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(19);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);

    //Salto de línea en celda
    $objPHPExcel ->getActiveSheet() ->getStyle('G2') ->getAlignment() ->setWrapText(true);
    $objPHPExcel ->getActiveSheet() ->getStyle('H2') ->getAlignment() ->setWrapText(true);
    $objPHPExcel ->getActiveSheet() ->getStyle('I2') ->getAlignment() ->setWrapText(true);
    $objPHPExcel ->getActiveSheet() ->getStyle('J2') ->getAlignment() ->setWrapText(true);
    $objPHPExcel ->getActiveSheet() ->getStyle('K2') ->getAlignment() ->setWrapText(true);
     
    cellColor('A1:G1', '626567');
    cellColor('H1:K1', '909497');
    cellColor('A2:K2', '2471a3');

        /*Extraer datos de MYSQL*/
        # Conectarse a la base de datos
        $con=@mysqli_connect('localhost', 'root', 'X3ryu5IPBX.TxMT3l3c0m');
        if(!$con){
            die("imposible conectarse: ".mysqli_error($con));
        }
        if (@mysqli_connect_errno()) {
            die("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
        }

        // FECHA
        date_default_timezone_set('Asia/Hong_Kong');
        $date = date("Y-n");  
                          

        // QUERY Date
            
            $queryReport="SELECT DISTINCT (omnifon.UniqueId),
						DATE_FORMAT(SUBSTRING(cdr.calldate, 1, 10),'%m-%d-%Y') As Date,
						SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',1),1,20) As Country,
						GROUP_CONCAT(omnifon.Pregunta, omnifon.Respuesta ORDER By omnifon.Pregunta) As Option_question
						FROM omnifon.Resultados As omnifon
						INNER JOIN asteriskcdrdb.cdr As cdr ON(cdr.uniqueid = omnifon.UniqueId)
						WHERE cdr.dcontext='Encuesta_Omnifon'
                        AND calldate >= '$date-01 00:00:00' AND calldate <= '$date-31 23:59:59'
                        GROUP BY UniqueId
                        ORDER BY cdr.calldate ASC";
                        $SQL1=mysqli_query($con,$queryReport);
                        $cel=3;
                        while ($resultqueryReport=mysqli_fetch_array($SQL1)){
                        $Date=$resultqueryReport['Date'];
                        $Country=$resultqueryReport['Country'];
                        $Option_question=$resultqueryReport['Option_question'];

                        $Q1 = substr($Option_question, 1, 1);
                        $Q2 = substr($Option_question, 4, 1);
                        $Q3 = substr($Option_question, 7, 1);
                        $Q4 = substr($Option_question, 10, 1);
                        $Q5 = substr($Option_question, 13, 1);

			    if($Q1 == "4" OR "5"){
                                $Q2 = 0;
                                $Q3 = 0;
                                $Q4 = 0;
                                $Q5 = 0;
                            }

                            if($Q1==""){
                                $Q1= "0";
                            }
                            if($Q2 == ""){
                                $Q2= "0";
                            }
                            if($Q3 == ""){
                                $Q3= "0";
                            }
                            if($Q4 == ""){
                                $Q4= "0";
                            }
                            if($Q5 == ""){
                                $Q5= "0";
                            }
           

            // Columnas utilizadas
            $a="A".$cel;
            $b="B".$cel;
            $c="C".$cel;
            $d="D".$cel;
            $e="E".$cel;
            $f="F".$cel;
            $g="G".$cel;
            $h="H".$cel;
            $i="I".$cel;
            $j="J".$cel;
            $k="K".$cel;

             $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a, 'IVR')
            ->setCellValue($b, $Date)
            ->setCellValue($c, 'China')
            ->setCellValue($d, $Country)
            ->setCellValue($e, 'Call Center(Omniphonia)')
            ->setCellValue($f, 'Canada')
            ->setCellValue($g, $Q1)
            ->setCellValue($h, $Q2)
            ->setCellValue($i, $Q3)
            ->setCellValue($j, $Q4)
            ->setCellValue($k, $Q5);

            $cel+=1;

        } 


    $rango="A1:K2";
       $styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN))
    );
    $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
    
?>
