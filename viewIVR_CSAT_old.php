<?php
    // FECHA Y HORA
    $date = $_GET['date'];
    $timeStart = $_GET['timeStart'];
    $timeEnd = $_GET['timeEnd'];
    $reporte = $_GET['reporte'];
?>

<!DOCTYPE html>
<html>
<?php
    session_start();
    $usuario = $_SESSION['username'];

    if(!isset($usuario)){
        header("location: login.php");
    }else{

    
?>
<head>
    <title>China - Reportes R4</title>
    <meta charset="utf-8">
    <meta name="author" content="Melissa Arellano,Edgar Flores">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- **** Iconos **** -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
        .label{
            font-family: 'Comfortaa', cursive;
        }
        .jumbotron {
            background-color: #eb984e;
            color: #fff;
            padding: 100px 25px;
            font-family: 'Comfortaa', cursive;
        }
        .bg-company-red {
            background-color: #8F949A; /*#c5c05 !important;*/
        }

    </style>
</head>
<!-- ***** NAVBAR MENU VERTICAL ***** -->
<!-- <nav class="navbar navbar-expand-lg navbar navbar-dark bg-custom"> -->
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-company-red">
    <img src="img/logo_txm.png" width="180px" height="40px">
    <span style="font-family: 'Comfortaa', cursive; font-size: 23px; font-weight: bold; color: #ffffff;">
        <!-- <label>&nbsp;&nbsp;&nbsp;Reports R1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
    </span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <span style="color: #ffffff;">&nbsp;&nbsp;&nbsp;
                    <i class="fas fa-user fa-1x"></i>
                </span>
                <span style="font-size: 18px; color: #ffffff;">
                    <label><?php echo $usuario ?></label>
                </span>

                </a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a href="logout.php">
                <span style="color: #ffffff;">
                        <i class="fas fa-sign-out-alt fa-2x"></i>
                </span>
                <!-- <span style="font-size: 12px; color: #ffffff;">
                    <label>Logout</label>
                </span> -->
            </a>      
        </form>
    </div>
</nav>

<!-- ***************** JUMBOTRON **************** -->
<div class="jumbotron text-center">
    <h1>Reports R4 - China </h1>
    <p></p>
</div><div class="container">
<?php
    }
?>


<body>
    <div class="container">
        <h3 class="text-center"> Report: IVR_CSAT <strong> <?php echo $date?> </strong> </h3>
        <br>
        <p>
        <div class="text-center">
            <a href="index.php" class="btn btn-info" id="submit"> <i class=""></i> Back </a>
            <a href="#" class="btn btn-success" id="submitExport">
                <i class="fa fa-download"></i> Export to Excel
            </a>
            </p>
            <form action="exportIVR_CSAT.php" method="post" target="_blank" id="formExport">
                <input type="hidden" id="data_to_send" name="data_to_send" />
            </form>
        </div>
        
        <div class="container">
        <div class="row d-flex justify-content-center">
            <table class="table table-hover table-bordered" border="1"  bordercolor="#666666" id="export_to_excel" style="border-collapse:collapse;">
            <thead>
                <tr>
                <th scope="row">Date</th>
                    <th>Time</th>
                    <th>Unique_id</th>
                    <th>Call_duration</th>
                    <th>Language</th>
                    <th>Country</th>
                    <th>Region</th>
                    <th>Location</th>
                    <th>Mission</th>
                    <th>Q1</th>
                    <th>Q2</th>
                    <th>Q3</th>
                    <th>Q4</th>
                    <th>Q5</th>
                </tr>
            </thead>
                <?php
                    include('conexion.php');
                        //SELECT PAISES
                        $resultadoPais =$mysqli -> query("SELECT
                        DISTINCT (omnifon.UniqueId),
                        SUBSTRING(cdr.calldate, 1, 10) As Date,
                        SUBSTRING(cdr.calldate, 12, 9) As Time,
                        cdr.uniqueid As Unique_id,
                        SEC_TO_TIME(cdr.duration) As Call_duration,
                        SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',-1),1,2) As Language,
                        SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',1),1,20) As Country,
                        GROUP_CONCAT(omnifon.Pregunta, omnifon.Respuesta ORDER By omnifon.Pregunta) As optionQuestion
                        FROM omnifonr4.Resultados As omnifon
                        INNER JOIN asteriskcdrdbr4.cdr As cdr ON(cdr.uniqueid = omnifon.UniqueId)
                        WHERE cdr.dcontext='Encuesta_Omnifon'
                        AND cdr.calldate BETWEEN '2019-02-11 00:00:59' AND '2019-02-11 11:59:59'
                        GROUP BY Unique_id
                        ORDER BY cdr.calldate ASC;");

                        while ($rowPais=mysqli_fetch_array($resultadoPais)){
                        $date= $rowPais["Date"];
                        $time= $rowPais['Time'];
                        $uniqueid= $rowPais['Unique_id'];
                        $callDuration = $rowPais['Call_duration'];
                        $language = substr(strrchr($rowPais['Language'], "-"), 1);
                        $country = $rowPais['Country'];
                        $optionQuestion = $rowPais['optionQuestion'];

                        $Q1 = substr($optionQuestion, 0, 1);
                        $A1 = substr($optionQuestion, 1, 1);

                        $Q2 = substr($optionQuestion, 3, 1);
                        $A2 = substr($optionQuestion, 4, 1);

                        $Q3 = substr($optionQuestion, 6, 1);
                        $A3 = substr($optionQuestion, 7, 1);

                        $Q4 = substr($optionQuestion, 9, 1);
                        $A4 = substr($optionQuestion, 10, 1);

                        $Q5 = substr($optionQuestion, 12, 1);
                        $A5 = substr($optionQuestion, 13, 1);

                        if($A1 == ""){
                            $A1 = 'NA';
                        }
                        if($A2 == ""){
                            $A2 = 'NA';
                        }
                        if($A3 == ""){
                            $A3 = 'NA';
                        }
                        if($A4 == ""){
                            $A4 = 'NA';
                        }
                        if($A5 == ""){
                            $A5 = 'NA';
                        }

                ?>
            <tbody>
                <tr>
                    <td><?php echo $date  ?></td>
                    <td><?php echo $time  ?></td>
                    <td><?php echo $uniqueid ?></td>
                    <td><?php echo $callDuration ?></td>
                    <td><?php echo $language ?></td>
                    <td><?php echo $country ?></td>
                    <td>R4</td>
                    <td>China</td>
                    <td>Canada</td>
                    <td><?php echo $A1 ?></td>
                    <td><?php echo $A2 ?></td>
                    <td><?php echo $A3 ?></td>
                    <td><?php echo $A4 ?></td>
                    <td><?php echo $A5 ?></td>
                </tr>
            </tbody>
            <?php
                    }
            ?>
        </table>
        </div>
        </div>

            <br/>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Bloque de anuncios adaptable -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-6676636635558550"
                 data-ad-slot="8523024962"
                 data-ad-format="auto"
                 data-full-width-responsive="true"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</div>

<script src="js/app.js"></script>
</body>
</html>
