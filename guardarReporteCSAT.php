<?php
    header("Location: index.php");
     
    /** Incluye PHPExcel */
    require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
    // Crear nuevo objeto PHPExcel
    $objPHPExcel = new PHPExcel();

    // Propiedades del documento
    $objPHPExcel->getProperties()->setCreator("Melissa Arellano, Edgar Flores")
                                 ->setLastModifiedBy("Melissa Arellano, Edgar Flores")
                                 ->setTitle("Office 2010 XLSX Documento de prueba")
                                 ->setSubject("Office 2010 XLSX Documento de prueba")
                                 ->setDescription("Documento de prueba SLA usando clases de PHP.")
                                 ->setKeywords("office 2010 openxml php")
                                 ->setCategory("Archivo con resultado de prueba");

    // FECHA
    date_default_timezone_set('Asia/Hong_Kong');
    //$date = '2020-01-03';
    $date = date("Y-n");   
    
    include('cellColor.php');
    // REPORTE CSAT
    include('IVR_CSAT_Report.php');
     


    // Cambiar el nombre de hoja de cálculo
    $objPHPExcel->getActiveSheet()->setTitle('R4_IVR_CSAT_Report_'.$date);

    // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
    $objPHPExcel->setActiveSheetIndex(0);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $name = '/var/www/html/ReportsR4/Reportes/R4_IVR_CSAT_Report_'.$date.'.xlsx';
    $objWriter->save($name);
    $objWriter->save('php://output');

    exit;

    ?> 