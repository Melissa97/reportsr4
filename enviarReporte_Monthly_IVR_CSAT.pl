#!/usr/bin/perl
use DBI;
use Email::Send::SMTP::Gmail;

my $path = "/var/www/html/ReportsR4/Reportes/";
#my $date = '2020-01-03';
my($day, $month, $year)=(localtime)[3,4,5];
$date = ($year+1900) . "-" . ($month+1) . "-" . $day ;

# ********************************************* FIN ********************************************************************

$message = "Hi,\n\nThis is the automatic monthly report for R4 IVR_CSAT Report on $date \n\nBest Regards.";

open(IN, "$path/R4_Monthly_IVR_CSAT_Report_$date.xlsx") or die "Couldn't open file, $!";
while($linea = <IN>) {
  # manipula los caracteres leidos y puestos en la variable
  # $linea. Por ejemplo convertir los caracteres a minúsculas
  $linea =~ tr/A-Z/a-z/;
  # escritura del resultado
  print OUT $linea;
}
close(IN);

print IVRCSS $message;

close(IVRCSS);

$IVRCSSAttachment = $path . "R4_Monthly_IVR_CSAT_Report_$date.xlsx";

my $att;
$att->[0]->{file}= $IVRCSSAttachment;

my ($mail,$error)=Email::Send::SMTP::Gmail->new( -smtp=>'smtp.gmail.com:587',
                                                 -login=>'novacloud@omnifon.com',
                                                 -pass=>'<=9QKh6P');
print "session error: $error" unless ($mail!=-1);

$mail->send(-to=>'eflores@txmglobal.com,marellano@txmglobal.com', -subject=>'R4 Monthly IVR_CSAT Report on ' . $date, -body=> $message, -attachmentlist=> $att);

#$mail->send(-to=>'csanchez@txmglobal.com,eflores@txmglobal.com,marellano@txmglobal.com,JavedS@vfsglobal.com,PoonamPa@Vfsglobal.com,DineshH@vfsglobal.com,Vinayakm@vfsglobal.com,KarishmaSi@Vfsglobal.com,SanjuS@vfsglobal.com,AkeelK@vfsglobal.com,SagarU@vfsglobal.com,SagarH@vfsglobal.com,NikitaG@Vfsglobal.com,RohanJ@vfsglobal.com,amitda@vfsglobal.com,ayelmani@htclimited.com,nmassimi@htclimited.com', -subject=>'R4 Monthly IVR_CSAT Report on ' . $date, -body=> $message, -attachmentlist=> $att);
$mail->bye;
