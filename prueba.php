
<head>
    <title>China - Reportes R4</title>
    <meta charset="utf-8">
    <meta name="author" content="Melissa Arellano,Edgar Flores">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- **** Iconos **** -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
        .label{
            font-family: 'Comfortaa', cursive;
        }
        .jumbotron {
            background-color: #eb984e;
            color: #fff;
            padding: 100px 25px;
            font-family: 'Comfortaa', cursive;
        }
        .bg-company-red {
            background-color: #8F949A; /*#c5c05 !important;*/
        }
        .th1{
            background: #909497;
            color: #fdfefe;
        }
        .th2{
            background: #2471a3;
            color: #fdfefe;
        }
        .th3{
            background: #626567 ;
            color: #fdfefe;
        }

    </style>
</head>

<!-- ***** NAVBAR MENU VERTICAL ***** -->
<!-- <nav class="navbar navbar-expand-lg navbar navbar-dark bg-custom"> -->
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-company-red">
    <img src="img/logo_txm.png" width="180px" height="40px">
    <span style="font-family: 'Comfortaa', cursive; font-size: 23px; font-weight: bold; color: #ffffff;">
        <!-- <label>&nbsp;&nbsp;&nbsp;Reports R1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
    </span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <span style="color: #ffffff;">&nbsp;&nbsp;&nbsp;
                    <i class="fas fa-user fa-1x"></i>
                </span>
                <span style="font-size: 18px; color: #ffffff;">
                    <label><?php echo $usuario ?></label>
                </span>

                </a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a href="logout.php">
                <span style="color: #ffffff;">
                        <i class="fas fa-sign-out-alt fa-2x"></i>
                </span>
                <!-- <span style="font-size: 12px; color: #ffffff;">
                    <label>Logout</label>
                </span> -->
            </a>      
        </form>
    </div>
</nav>


<!-- ***************** JUMBOTRON **************** -->
<div class="jumbotron text-center">
    <h1>Reports R4 - China </h1>
    <p></p>
</div>
<?php
    // }
?>
<!-- ***************** JUMBOTRON **************** -->

<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<div class="row">
  <div class="col-6 col-md-1"></div>
  <div class="col-6 col-md-10"><table class="table table-hover table-bordered" border="1" bordercolor="#666666" id="export_to_excel" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <th colspan="7" class=" th3 text-center">IVR Questions</th>
                        <th class="th1 text-center">Knowledge</th>
                        <th class="th1 text-center">Experience with Staff</th>
                        <th class="th1 text-center">Courtesy</th>
                        <th class="th1 text-center">Waiting Time</th>
                    </tr>
                    <tr>
                        <th class="th2 text-center">Source Type</th>
                        <th class="th2 text-center">Response Date (MM-dd-YYY)<br/> </th>
                        <th class="th2 text-center">Region</th>
                        <th class="th2 text-center">Country</th>
                        <th class="th2 text-center">Location</th>
                        <th class="th2 text-center">Mission</th>
                        <th class="th2 text-center">Overall Rating</th>
                        <th class="th2 text-center">The contact centre officer was knowledgeable and well trained</th>
                        <th class="th2 text-center">The contact centre officer understood my issue</th>
                        <th class="th2 text-center">The contact centre officer was courteous and polite</th>
                        <th class="th2 text-center">Once connected to the contact centre officer, Your call was handled at an appropriate speed</th>
                    </tr>
                </thead>
                    <?php
                        include('conexion.php');
                            //SELECT PAISES
                            $resultadoPais =$mysqli -> query("SELECT
                            DISTINCT (omnifon.UniqueId),
                            SUBSTRING(cdr.calldate, 1, 10) As Date,
                            SUBSTRING(cdr.calldate, 12, 9) As Time,
                            cdr.uniqueid As Unique_id,
                            SEC_TO_TIME(cdr.duration) As Call_duration,
                            SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',-1),1,2) As Language,
                            SUBSTRING(SUBSTRING_INDEX(cdr.userfield,'-',1),1,20) As Country,
                            GROUP_CONCAT(omnifon.Pregunta, omnifon.Respuesta ORDER By omnifon.Pregunta) As optionQuestion
                            FROM omnifonr4.Resultados As omnifon
                            INNER JOIN asteriskcdrdbr4.cdr As cdr ON(cdr.uniqueid = omnifon.UniqueId)
                            WHERE cdr.dcontext='Encuesta_Omnifon'
                            AND cdr.calldate BETWEEN '2019-02-11 00:00:59' AND '2019-02-11 08:59:59'
                            GROUP BY Unique_id
                            ORDER BY cdr.calldate ASC;");

                            while ($rowPais=mysqli_fetch_array($resultadoPais)){
                            $date= $rowPais["Date"];
                            $newDate = date("m-d-Y", strtotime($date));

                            $time= $rowPais['Time'];
                            $uniqueid= $rowPais['Unique_id'];
                            $callDuration = $rowPais['Call_duration'];
                            $language = substr(strrchr($rowPais['Language'], "-"), 1);
                            $country = $rowPais['Country'];
                            $optionQuestion = $rowPais['optionQuestion'];

                            $Q1 = substr($optionQuestion, 0, 1);
                            $A1 = substr($optionQuestion, 1, 1);

                            $Q2 = substr($optionQuestion, 3, 1);
                            $A2 = substr($optionQuestion, 4, 1);

                            $Q3 = substr($optionQuestion, 6, 1);
                            $A3 = substr($optionQuestion, 7, 1);

                            $Q4 = substr($optionQuestion, 9, 1);
                            $A4 = substr($optionQuestion, 10, 1);

                            $Q5 = substr($optionQuestion, 12, 1);
                            $A5 = substr($optionQuestion, 13, 1);

                            if($A1 == ""){
                                $A1 = 'NA';
                            }
                            if($A2 == ""){
                                $A2 = 'NA';
                            }
                            if($A3 == ""){
                                $A3 = 'NA';
                            }
                            if($A4 == ""){
                                $A4 = 'NA';
                            }
                            if($A5 == ""){
                                $A5 = 'NA';
                            }

                    ?>
                <tbody>
                    <tr>
                        <td>IVR</td>
                        <td><?php echo $newDate ?></td>
                        <td>Americas</td>
                        <td><?php echo $country ?></td>
                        <td>Call Center (Omniphonia)</td>
                        <td>Canada</td>
                        <td><?php echo $A1 ?></td>
                        <td><?php echo $A2 ?></td>
                        <td><?php echo $A3 ?></td>
                        <td><?php echo $A4 ?></td>
                        <td><?php echo $A5 ?></td>
                    </tr>
                </tbody>
                <?php
                        }
                ?>
            </table></div>
  <div class="col-6 col-md-1">
  </div>
</div>


        
            
        

