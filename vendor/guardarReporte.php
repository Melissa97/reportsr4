<?php
    header("Location: index.php");
     
    /** Incluye PHPExcel */
    require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
    // Crear nuevo objeto PHPExcel
    $objPHPExcel = new PHPExcel();

    // Propiedades del documento
    $objPHPExcel->getProperties()->setCreator("Melissa Arellano, Edgar Flores")
                                 ->setLastModifiedBy("Melissa Arellano, Edgar Flores")
                                 ->setTitle("Office 2010 XLSX Documento de prueba")
                                 ->setSubject("Office 2010 XLSX Documento de prueba")
                                 ->setDescription("Documento de prueba SLA usando clases de PHP.")
                                 ->setKeywords("office 2010 openxml php")
                                 ->setCategory("Archivo con resultado de prueba");

    // FECHA
    $date = 'NOW()';

    //-----------------------------------------------------------------------------------------------------------------------------------------
     
    // REPORTE DE SLA
    include('SLA.php');
     
    //-----------------------------------------------------------------------------------------------------------------------------------------

    // REPORTE DE IVR
    include('IVR.php');
    
    //-----------------------------------------------------------------------------------------------------------------------------------------
     
    // REPORTE DE PBX
    include('PBX.php');
    
    //-----------------------------------------------------------------------------------------------------------------------------------------
     
    // REPORTE DE PBX CDR DETAILS
    // include('PBX_CDR_Details.php');
    
    //-----------------------------------------------------------------------------------------------------------------------------------------

    // Cambiar el nombre de hoja de cálculo
    $objPHPExcel->getActiveSheet()->setTitle('SLA');

    // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
    $objPHPExcel->setActiveSheetIndex(0);

    // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
    // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    // header('Content-Disposition: attachment;filename="pruebaReal.xlsx"');
    // header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $name = 'C:xampp/htdocs/ReportesR1/Reportes/Reporte_'.$date.'.xlsx';
    $objWriter->save($name);
    $objWriter->save('php://output');

    exit;

    ?> 