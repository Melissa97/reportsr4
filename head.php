<!DOCTYPE html>
<html>
<?php
    session_start();
    $usuario = $_SESSION['username'];

    if(!isset($usuario)){
        header("location: login.php");
    }else{

    
?>
<head>
    <title>China - Reportes R4</title>
    <meta charset="utf-8">
    <meta name="author" content="Melissa Arellano,Edgar Flores">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- **** Iconos **** -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
        .label{
            font-family: 'Comfortaa', cursive;
        }
        .jumbotron {
            background-color: #eb984e;
            color: #fff;
            padding: 90 15px;
            font-family: 'Comfortaa', cursive;
        }
        .bg-company-red {
            background-color: #8F949A; /*#c5c05 !important;*/
        }
        .th1{
            background: #909497;
            color: #fdfefe;
            font-size: 15px;
        }
        .th2{
            background: #2471a3;
            color: #fdfefe;
            font-size: 15px;
        }
        .th3{
            background: #626567 ;
            color: #fdfefe;
            font-size: 13px;
        }
        .td1{
            font-size: 16px;
        }

    </style>
</head>

<!-- ***** NAVBAR MENU VERTICAL ***** -->
<!-- <nav class="navbar navbar-expand-lg navbar navbar-dark bg-custom"> -->
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-company-red">
    <img src="img/logo_txm.png" width="180px" height="40px">
    <span style="font-family: 'Comfortaa', cursive; font-size: 23px; font-weight: bold; color: #ffffff;">
        <!-- <label>&nbsp;&nbsp;&nbsp;Reports R1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
    </span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <span style="color: #ffffff;">&nbsp;&nbsp;&nbsp;
                    <i class="fas fa-user fa-1x"></i>
                </span>
                <span style="font-size: 18px; color: #ffffff;">
                    <label><?php echo $usuario ?></label>
                </span>

                </a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a href="logout.php">
                <span style="color: #ffffff;">
                        <i class="fas fa-sign-out-alt fa-2x"></i>
                </span>
                <!-- <span style="font-size: 12px; color: #ffffff;">
                    <label>Logout</label>
                </span> -->
            </a>      
        </form>
    </div>
</nav>


<!-- ***************** JUMBOTRON **************** -->
<div class="jumbotron text-center">
    <h1>Reports R4 - China </h1>
    <p></p>
</div>
<?php
    }
?>
<!-- ***************** JUMBOTRON **************** -->
